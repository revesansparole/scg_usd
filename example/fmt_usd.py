import json

from scenegraph.io import loads

from scg_usd.exporter import export, save_stage

sc = loads(json.load(open("sc.json")))

stage = export(sc, "toto.usda")
save_stage(stage)
