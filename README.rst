========================
scg_usd
========================

.. {# pkglts, doc

.. image:: https://revesansparole.gitlab.io/scg_usd/_images/badge_pkging_pip.svg
    :alt: PyPI version
    :target: https://pypi.org/project/scg_usd/0.0.1/

.. image:: https://revesansparole.gitlab.io/scg_usd/_images/badge_pkging_conda.svg
    :alt: Conda version
    :target: https://anaconda.org/revesansparole/scg_usd

.. image:: https://revesansparole.gitlab.io/scg_usd/_images/badge_doc.svg
    :alt: Documentation status
    :target: https://revesansparole.gitlab.io/scg_usd/

.. image:: https://badge.fury.io/py/scg_usd.svg
    :alt: PyPI version
    :target: https://badge.fury.io/py/scg_usd

.. #}
.. {# pkglts, glabpkg_dev, after doc

.. #}

Convert between scenegraph and usd format

