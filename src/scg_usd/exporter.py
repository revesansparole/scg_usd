"""
Export scenegraph as usd document
"""
from math import degrees

import numpy as np
from pxr import Sdf, Usd, UsdGeom, UsdShade
from scenegraph import Rotation, RotationEuler, Scaling, Translation
from scenegraph.algo import transfo_tot


def usd_mesh(mesh, prim):
    """Convert mesh into usd

    Args:
        mesh (TriangleSet):
        prim (UsdGeom.Mesh): usd primitiv to fill

    Returns:
        (None)
    """
    pts = np.array(mesh.points())
    bb_min = pts.min(axis=0, initial=np.inf)
    bb_max = pts.max(axis=0, initial=-np.inf)

    prim.CreatePointsAttr([pt.tolist() for pt in mesh.points()])
    prim.CreateFaceVertexCountsAttr([3] * len(mesh.faces()))
    prim.CreateFaceVertexIndicesAttr([ind for face in mesh.faces() for ind in face])
    prim.CreateExtentAttr([tuple(bb_min), tuple(bb_max)])


def usd_mat(col, name, stage):
    """Create usd material.

    Args:
        col (str): #rrggbb
        name (str): usd name of material
        stage (Usd.Stage): doc

    Returns:
        (UsdShade.Material)
    """
    mat = UsdShade.Material.Define(stage, f'{name}')
    pbr = UsdShade.Shader.Define(stage, f'{name}/pbr_shader')
    pbr.CreateIdAttr("UsdPreviewSurface")
    col_tup = tuple(int(col[1 + 2 * i:1 + 2 * (i + 1)], 16) / 255. for i in range(3))
    pbr.CreateInput("diffuseColor", Sdf.ValueTypeNames.Float3).Set(col_tup)
    pbr.CreateInput("roughness", Sdf.ValueTypeNames.Float).Set(0.4)
    pbr.CreateInput("metallic", Sdf.ValueTypeNames.Float).Set(0.0)

    mat.CreateSurfaceOutput().ConnectToSource(pbr.ConnectableAPI(), "surface")

    return mat


def export_transfo(transfo, prim):
    """Export transfo to usd format.

    Args:
        transfo (Transfo):
        prim (UsdGeom.Mesh): usd primitive to transform

    Returns:
        (None)
    """
    if isinstance(transfo, Translation):
        prim.AddTranslateOp().Set(tuple(transfo.vec))
    elif isinstance(transfo, Rotation):
        axis = transfo.axis
        if isinstance(axis, str):
            if axis == 'Ox':
                prim.AddRotateXOp().Set(degrees(transfo.alpha))
            elif axis == 'Oy':
                prim.AddRotateYOp().Set(degrees(transfo.alpha))
            elif axis == 'Oz':
                prim.AddRotateZOp().Set(degrees(transfo.alpha))
            else:
                raise NotImplementedError(f"unrecognized axis descr '{axis}")
        else:
            pass  # TODO raise NotImplementedError("only rotation along primary axes")
    elif isinstance(transfo, RotationEuler):  # TODO debug order of euler angles
        prim.AddRotateYOp().Set(degrees(transfo.yaw))
        prim.AddRotateXOp().Set(degrees(transfo.roll))
        prim.AddRotateZOp().Set(degrees(transfo.pitch))
        # prim.AddRotateZXYOp().Set((degrees(transfo.pitch), degrees(transfo.roll), degrees(transfo.yaw)))
    elif isinstance(transfo, Scaling):
        prim.AddScaleOp().Set(tuple(transfo.vec))
    else:
        raise NotImplementedError(f"unrecognized transfo '{transfo}")


def _find_color(node):
    try:
        return node.meta['color']
    except KeyError:
        if node.parent is None:
            return 'default'
        else:
            return _find_color(node.parent)


def export_node(node, name, stage, mats, no_transfo=False):
    """Export node as usd.

    Args:
        node (ScNode):
        name (str): name to use in usd
        stage (Usd.Stage): doc
        mats (dict): armory of materials
        no_transfo (bool): whether to export transfo list or apply it on geometry

    Returns:
        (None)
    """
    prim = UsdGeom.Mesh.Define(stage, name)
    if node.shape is not None:
        shp = node.get_shape_inst()
        try:
            color = shp.meta['color']
        except KeyError:
            color = _find_color(node)

        try:
            mat = mats[color]
        except KeyError:
            mat = usd_mat(color, f"/mat/h{color[1:]}", stage)
            mats[color] = mat

        UsdShade.MaterialBindingAPI(prim).Bind(mat)

        if no_transfo:
            shp = shp.copy()
            tr = transfo_tot(node, absolute=True)
            shp.apply(tr)

        usd_mesh(shp, prim)

    if not no_transfo:
        for transfo in reversed(node.transfos):
            export_transfo(transfo, prim)

    for i, child in enumerate(node.children):
        try:
            chname = child.meta['name']
        except KeyError:
            chname = f"ch{i:d}"

        export_node(child, f"{name}/{chname}", stage, mats, no_transfo)


def export(scene, pth=None, no_transfo=False):
    """Export full scene.

    Notes: expand all clones as single shapes

    Args:
        scene (Scene): object to export
        pth (str): name of scene
        no_transfo (bool): whether to export transfo list or apply it on geometry

    Returns:
        (Usd.Stage)
    """
    if pth is None:
        try:
            pth = scene.meta['name']
        except KeyError:
            pth = "scene"

    if not pth.endswith(".usda"):
        pth = f"{pth}.usda"

    stage = Usd.Stage.CreateNew(pth)
    stage.SetMetadata('comment', 'Export from scenegraph')
    UsdGeom.SetStageUpAxis(stage, UsdGeom.Tokens.z)

    mats = dict(default=usd_mat('#aaaaaa', '/mat/default', stage))

    name = "/sc"
    prim = UsdGeom.Mesh.Define(stage, name)
    if not no_transfo:
        for transfo in reversed(scene.transfos):
            export_transfo(transfo, prim)

    for i, child in enumerate(scene.children):
        try:
            chname = child.meta['name']
        except KeyError:
            chname = f"ch{i:d}"

        export_node(child, f"{name}/{chname}", stage, mats, no_transfo)

    return stage


def save_stage(stage):
    """Save previously defined stage.

    Args:
        stage (Usd.Stage): see export_scene

    Returns:
        None
    """
    stage.GetRootLayer().Save()
